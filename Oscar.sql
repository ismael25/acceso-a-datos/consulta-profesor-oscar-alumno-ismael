﻿-- Listar el nombre y el dorsal de los ciclistas de mas de 30 años que hayan ganado etapas

  -- (Join)

    SELECT  DISTINCT (c.dorsal) , nombre FROM ciclista c  INNER JOIN etapa e USING (dorsal) WHERE edad > 30;

  -- (IN)

     SELECT c.dorsal , c.nombre  FROM ciclista c  
              WHERE (dorsal IN (SELECT dorsal FROM etapa) AND (edad >30)); 